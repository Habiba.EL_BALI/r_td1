################################################################################
######################### TD 2 DATA MANAGEMENT #################################
################################################################################

# Septembre 2022

# nouveau RProject à enregistrer dans le même dossier que les données
# + ouvrir et modifier ce script (enregistré dans le même dossier également)
################################################################################
## 1 Import data
source("path.R")

path_data = "/Users/elbali/r_td1/data"
path_data

pop_insee = read.csv2(file.path(path_data,"BTT_TD_POP1B_2017.csv"))
deces_insee = read.csv2(file.path(path_data,"deces-2017.csv"))
immig_insee = readxl::read_xlsx(file.path(path_data,"BTX_TD_IMG1A_2017.xlsx"))
profes_insee  = readxl::read_xlsx(file.path(path_data,"TCRD_005.xlsx"))
  

# la fonction readxl::read_xlsx contient un paramètre 'skip' 
#                       qui permet de lire le fichier à partir d'une certaine ligne
 

library(readxl) 
library(dplyr) 
library(lubridate) 
library (tidyr)
library(stringr)

########################################################################
 #Nettoyage des données

########################################################################
##################### DATA MANAGEMENT ##########################################
# analyser la structure des 4 datasets, les comprendre, identifier les changements à faire, merge etc..

## Description de nos bases de données 

str(pop_insee)
str(deces_insee)
str(immig_insee)
str(profes_insee)


##################### DATA MANAGEMENT DE LA TABLE POP_INSEE ###################################


#passer les noms de colonnes en minuscule 

colnames(pop_insee)= tolower((colnames(pop_insee)))

# Transformation variable nb "chr" en varialbe integer
pop_insee$nb <-as.integer(pop_insee$nb)

#Filtrage par département et non par arrondissement et creation de la data.frame

pop_dep = pop_insee %>%
  filter(nivgeo != "ARM") %>%
  mutate(departement= substr(codgeo,1,2),# extraire 2 première chr du dep
         somme_age=aged100*nb) %>%    # et somme d'age pop par dep
  filter(departement != "") %>%
  group_by(departement)  %>%  # regroupement par dep et retrait valeurs manquantes
  summarise(
    nb_habitants = round(sum(nb, na.rm=TRUE)),
    nb_hommes = round(sum(nb[sexe == 1], na.rm = TRUE)),
    nb_femmes = round(sum(nb[sexe == 2], na.rm = TRUE)),
    somme_age = round(sum(somme_age, na.rm = TRUE)),
) %>%
  mutate(age_moyen_vivant = round((somme_age / nb_habitants), 2))
str(pop_dep)

##################### DATA MANAGEMENT DE LA TABLE DECES_INSEE ##########################################

# Changement du format des dates

deces_insee$datenaiss = ymd(deces_insee$datenaiss)
deces_insee$datedeces = ymd(deces_insee$datedeces)

deces_insee$age_deces = round(difftime(deces_insee$datedeces, 
                                       deces_insee$datenaiss, units = "days")/365)
 
###### Retirer l'unite days pour l'age de deces

deces_insee<-deces_insee %>% 
  dplyr:: mutate(age_deces = str_replace(age_deces, pattern = "days", replacement = ""))

###### Passage de la variable "age_deces" en tant que int  

deces_insee$age_deces = as.integer(deces_insee$age_deces)

##### Ajout de la colonne departement de naissance et de deces (elimination des paysnaiss etranger)
##### calcul du nb d'hommes, femmes , habitant et age moyen de deces par departement

deces_dep = deces_insee %>%
  filter(paysnaiss == "") %>%
  mutate(departement_naiss = substr(lieunaiss, 1, 2),
                departement = substr(lieudeces, 1, 2)) %>%
  filter(departement != "") %>%
  group_by(departement) %>%
  summarise(
    nb_femmes = round(sum(sexe == 2, na.rm = TRUE)),
    nb_hommes = round(sum(sexe == 1, na.rm = TRUE)),
    nb_habitants = round(sum(nb_hommes,nb_femmes, na.rm=TRUE)),
    somme_age = round(sum(age_deces, na.rm = TRUE))) %>%
  mutate(age_moyen_deces = round((somme_age / nb_habitants), 2))

deces_dep

##################### Fichier immig_insee#######################################

#transformer les noms de colonnes en minisucule 

colnames(immig_insee) = tolower(colnames(immig_insee)) 

## Ajouter une colonne pour le département
## On veut le taux d"immigres par dep independamment du sexe et age
## Faire la somme des immigres par dep et somme des non immigrés par dep

immig_dep = immig_insee %>%
  mutate(departement = substr(codgeo, 1, 2))%>%
  filter(departement != "") %>%
  group_by(departement) %>%
  summarise(
    age400_immi1_sexe1 = round(sum(age400_immi1_sexe1, na.rm = TRUE)),
    age400_immi1_sexe2 = round(sum(age400_immi1_sexe2, na.rm = TRUE)),
    age400_immi2_sexe1 = round(sum(age400_immi2_sexe1, na.rm = TRUE)),
    age400_immi2_sexe2 = round(sum(age400_immi2_sexe2, na.rm = TRUE)),
    age415_immi1_sexe1 = round(sum(age415_immi1_sexe1, na.rm = TRUE)),
    age415_immi1_sexe2 = round(sum(age415_immi1_sexe2, na.rm = TRUE)),
    age415_immi2_sexe1 = round(sum(age415_immi2_sexe1, na.rm = TRUE)),
    age415_immi2_sexe2 = round(sum(age415_immi2_sexe2, na.rm = TRUE)),
    age425_immi1_sexe1 = round(sum(age425_immi1_sexe1, na.rm = TRUE)),
    age425_immi1_sexe2 = round(sum(age425_immi1_sexe2, na.rm = TRUE)),
    age425_immi2_sexe1 = round(sum(age425_immi2_sexe1, na.rm = TRUE)),
    age425_immi2_sexe2 = round(sum(age425_immi2_sexe2, na.rm = TRUE)),
    age455_immi1_sexe1 = round(sum(age455_immi1_sexe1, na.rm = TRUE)),
    age455_immi1_sexe2 = round(sum(age455_immi1_sexe2, na.rm = TRUE)),
    age455_immi2_sexe1 = round(sum(age455_immi2_sexe1, na.rm = TRUE)),
    age455_immi2_sexe2 = round(sum(age455_immi2_sexe2, na.rm = TRUE)),
    nb_immig = round(sum(age400_immi1_sexe1,
                         age400_immi1_sexe2,
                         age415_immi1_sexe1,
                         age415_immi1_sexe2,
                         age425_immi1_sexe1,
                         age425_immi1_sexe2,
                         age455_immi1_sexe1,
                         age455_immi1_sexe2, 
                         na.rm = TRUE)),
    nb_non_immig = round(sum(age400_immi2_sexe1,
                             age400_immi2_sexe2,
                             age415_immi2_sexe1,
                             age415_immi2_sexe2,
                             age425_immi2_sexe1,
                             age425_immi2_sexe2,
                             age455_immi2_sexe1,
                             age455_immi2_sexe2, 
                             na.rm = TRUE)),
    nb_habitants = round(sum(nb_immig,
                             nb_non_immig, 
                             na.rm=TRUE))) %>%
  mutate(taux_img = round((nb_immig *100/ nb_habitants), 2))



##################### DATA MANAGEMENT DE LA TABLE PROFES_INSEE ##########################################

##Renommer les colonnes pour la table profes_insee

colnames(profes_insee) = c('departement', 'lib_departement', 'taux_agriculteurs_exploitants', 
                           'taux_artisans_commer?ants_chef_entreprises', 'taux_cadres', 
                           'taux_profs_intermediaires', 'taux_employ?s', 'taux_ouvriers', 
                           'taux_retrait?s', 'taux_sans_act_pro')


#Retire les 3ere lignes de la table profes_insee (na et titres des colonnes noter en double ( cause de la manoeuvre précédente) dans les cellules ont été supprimer)

profes_insee <- profes_insee %>%
  filter(!row_number() %in% c(1, 2, 3))

#Selection de toutes les colonnes sauf "lib_departement"

profes_dep = profes_insee %>%
  select(-lib_departement)

##################### MERGE DES DATAFRAMES :LE TAUX D'HOMMES VIVANTS ##########################################

data1=merge(pop_dep,deces_dep, by = "departement")

data1=data1%>%
  mutate(taux_hommes_vivants = round((nb_hommes.x *100/ (nb_hommes.x + nb_hommes.y)), 2))

#Selection d'uniquement les colonnes pertientes a l'obtention du tableau voulu 

data1 = data1 %>%
  select(departement,age_moyen_vivant, taux_hommes_vivants, age_moyen_deces)



##################### MERGE DES DATAFRAMES ##########################################

data2=merge(immig_dep,profes_dep, by = "departement")

data_final=merge(data1,data2, by = "departement")

##################### EXPORT DU FICHIER AU FORMAT CSV ##########################################

write.csv2(data_final, file = file.path(path_export,"r_td1.csv"), row.names = FALSE)

